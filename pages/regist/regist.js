// pages/regist/regist.js

var app = getApp();
var api = require('../../utils/api.js');
var util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    has_get_authCode: false,
    authCodeGetTime: 0,
    name: "",
    phone: "",
    pwd: "",
    code: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  nameInput: function (event) {
    this.setData({
      name: event.detail.value
    })
  },

  phoneNumInput: function (event) {
    this.setData({
      phone: event.detail.value
    })
  },

  passwordInput: function (event) {
    this.setData({
      pwd: event.detail.value
    })
  },

  codeInput: function (event) {
    this.setData({
      code: event.detail.value
    })
  },

  tapGetAuthCode: function (event) {
    var self = this;
    if (self.data.phone.length > 0) {
      self._initAuthCodeTimer();
      //获取短信验证码
      self.getAuthCode();
    } else {
      wx.showModal({
        title: '温馨提示',
        showCancel: false,
        content: '请填写正确的手机号',
      });
      return false;
    }
  },

  getAuthCode: function () {
    var self = this;

    var params = {
      data: {
        VerifyType: 0,
        phone: self.data.phone,
        Type: 1,
        SMSType: 0,
      },
    };
    api.methodWechatGetSMSCode({
      params,
      success: (res) => {
        console.log(res);
        if (res.data.code != 0) {
          wx.showModal({
            title: '温馨提示',
            showCancel: false,
            content: res.data.msg,
          });
          return;
        } else {
          wx.showToast({
            title: '发送成功',
          })
        }
      },
      fail: (res) => {
        console.log(res);
        wx.showModal({
          title: "温馨提示",
          showCancel: false,
          content: "获取验证码失败，请稍后重试！",
        });
      },
      complete: (res) => {
        console.log(res);
      }
    });
  },

  //60s倒计时
  _initAuthCodeTimer: function () {
    var self = this;
    var initTime = 60;
    self.setData({
      has_get_authCode: true,
      authCodeGetTime: initTime
    });

    var authCodeTimer = setInterval(function () {
      initTime--;
      self.setData({
        authCodeGetTime: initTime
      });
      if (initTime <= 0) {
        clearInterval(authCodeTimer);
        self.setData({
          has_get_authCode: false
        });
      }
    }, 1000);
  },


  tapRegist: function (event) {
    var self = this;

    if (this.data.name.length <= 0) {
      wx.showModal({
        title: '温馨提示',
        showCancel: false,
        content: '名字不能为空',
      })
      return;
    }

    if (this.data.phone.length <= 0) {
      wx.showModal({
        title: '温馨提示',
        showCancel: false,
        content: '手机号码不能为空',
      })
      return;
    }

    if (this.data.pwd.length <= 0) {
      wx.showModal({
        title: '温馨提示',
        showCancel: false,
        content: '密码不能为空',
      })
      return;
    }

    if (this.data.code.length <= 0) {
      wx.showModal({
        title: '温馨提示',
        showCancel: false,
        content: '验证码不能为空',
      })
      return;
    }

    //注册接口
    var params = {
      data: {
        AppKey: "049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4",
        phone: self.data.phone,
        Code: self.data.code,
        UserName: self.data.name,
        UserPwd: self.data.pwd,
        Source: "MiniappTTKYX_578009 578009",
        SourceType: 2,
        OpenId: app.globalData.openId,
      },
    };

    wx.showLoading({
      title: "加载中",
    })
    
    api.methodWechatRegester({
      params,
      success: (res) => {
        console.log(res);
        wx.hideLoading();
        if (res.data.code != 0) {
          wx.showModal({
            title: '温馨提示',
            showCancel: false,
            content: res.data.msg,
          })
          return;
        } else {
          //注册成功
          wx.showToast({
            title: '注册成功!',
          });

          //注册成功后直接调用登录接口 
          self.nextLogin();
        }
      },
      fail: (res) => {
        console.log(res);
        wx.hideLoading();
      },
      complete: (res) => {
        wx.hideLoading();
      }
    });
  },

  nextLogin: function () {

    var self = this;

    var frams = {
      data: {
        AppKey: "049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4",
        UserPwd: self.data.pwd,
        UserAccount: self.data.phone,
        OpenId: app.globalData.openId,
      },
    };

    api.methodWechatLogin({
      frams,
      success: (res) => {
        console.log(res);
        wx.hideLoading();

        if (res.data.code != 0) {
          wx.showModal({
            title: '温馨提示',
            showCancel: false,
            content: res.data.msg,
          })
          return;
        } else {
          //登录成功后，同步用户数据，返回个人中心界面
          app.globalData.loginStatus = true;
          wx.switchTab({
            url: '../my/my',
          })
        }
      },
      fail: (res) => {
        console.log(res);
        wx.showModal({
          title: '温馨提示',
          showCancel: false,
          content: '自动登录失败，请返回登录界面重新登录！',
          success: function (res) {
            if (res.confirm) {
              wx.switchTab({
                url: '../my/my',
              })
            }
          }
        });
      },
      complete: (res) => {
        console.log(res);
      }
    });
  },
})