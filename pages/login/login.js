// pages/login/login.js

var app = getApp();
var api = require('../../utils/api.js');
var util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: "",
    pwd: "",
    url: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  phoneNumInput: function (event) {
    this.setData({
      phone: event.detail.value
    })
  },

  passwordInput: function (event) {
    this.setData({
      pwd: event.detail.value
    })
  },

  tapLogin: function () {
    var self = this;

    if (this.data.phone.length <= 0) {
      wx.showModal({
        title: '温馨提示',
        showCancel:false,
        content: '手机号码不能为空',
      })
      return;
    }

    if (this.data.pwd.length <= 0) {
      wx.showToast({
        title: '登录密码不能为空！',
      })
      return;
    }

    wx.showLoading({
      title: '登录中...',
    })

    var data = {
      phone: self.data.phone,
      password: self.data.pwd,
    };
 
    var frams = {
      data: {
        AppKey: "049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4",
        UserPwd: self.data.pwd,
        UserAccount: self.data.phone,
        OpenId: app.globalData.openId,
      },
    };

    api.methodWechatLogin({
      frams,
      success: (res) => {
        console.log(res);
        wx.hideLoading();

        if (res.data.code != 0) {
          wx.showModal({
            title: '温馨提示',
            showCancel: false,
            content: res.data.msg,
          })
          return;
        } else {
          //登录成功后，同步用户数据，返回个人中心界面
          app.globalData.loginStatus = true;
          wx.switchTab({
            url: '../my/my',
          })
        }
      },
      fail: (res) => {
        console.log(res);
        wx.showModal({
          title: '温馨提示',
          showCancel: false,
          content: '自动登录失败，请返回登录界面重新登录！',
          success: function (res) {
            if (res.confirm) {
              wx.switchTab({
                url: '../my/my',
              })
            }
          }
        });
      },
      complete: (res) => {
        console.log(res);
      }
    });
  }
})