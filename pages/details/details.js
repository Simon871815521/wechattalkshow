// pages/details/details.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    "src": "http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400",
    "RemainingTime": 10,
    "isStart": false,
    "isPlay": false,
    "isComplete": false,
    "showScore": false,
    "isEnd": false,
    "index": 0,
    "score": 80,
    "subtitleList": [
      { "timeInterval": 5000, "en": "You're wondering why I brought you all here.", "ch": "你们一定很奇怪 我为什么召集你们来这里" },
      { "timeInterval": 5000, "en": "After all, we just had a feast.", "ch": "毕竟我们才举办过一次盛宴" },
      { "timeInterval": 5000, "en": "Since when does old Walder give us two feasts in a single fortnight?", "ch": "老瓦德怎么会在十几天内连续举办两场盛宴" },
      { "timeInterval": 5000, "en": "Well, it's no good being Lord of the Riverlands", "ch": "如果不能和家人一起庆祝" },
      { "timeInterval": 5000, "en": "if you can't celebrate with your family. That's what I say!", "ch": "身为河间地的领主有什么意义，那就是我的看法" },
      { "timeInterval": 5000, "en": "I've gathered every Frey who means a damn thing", "ch": "我召集了弗雷家族 每一个有点出息的人" },
    ]
  },
  // 开始录音函数
  startRecord: function () {
    if (!this.data.isPlay) {
      // 开始录音，调用录音接口函数
      wx.startRecord({
        success: function (res) {
          var tempFilePath = res.tempFilePath
        },
        fail: function (res) {
          //录音失败
          // wx.showModal({
          //     title: '提示',
          //     content: '录音功能没有授权，点击确定授权',
          //     success: function(res) {
          //         if (res.confirm) {
          //             console.log('用户点击确定')
          //         } else if (res.cancel) {
          //             console.log('用户点击取消')
          //         }
          //     }
          // })
        }
      })

      // 点击录音按钮后设置页面状态
      this.setData({
        "isPlay": true,
        "isStart": true,
        "showScore": false,
      });
      // 开定时器，记录录音时间
      var that = this;
      var timer = setInterval(function () {
        that.setData({
          "RemainingTime": that.data.RemainingTime - 1
        });

        // 当剩余时间为零时，停止录音
        if (that.data.RemainingTime == 0) {
          clearInterval(timer);
          that.setData({
            "isPlay": false,

          });
          //结束录音  
          wx.stopRecord();

          // 显示加载中
          wx.showLoading({
            title: 'Ai老师测评中...',
          })

          // 上传数据到后台获取得分信息
          setTimeout(function () {

            wx.hideLoading();
            that.setData({
              "showScore": true,
              "isComplete": true,
            });

            // 显示得分2秒钟
            setTimeout(function () {
              that.setData({
                "showScore": false,
              })
            }, 2000)
          }, 2000)

          // 如果测评结果获取失败
          function getFail() {
            wx.showModal({
              title: '提示',
              content: '测评失败，请重录',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }



        }
      }, 1000);

    }
  },

  // 停止录音函数
  stopRecord: function () {

    console.log('stopRecord');
    var that = this;
    that.setData({
      "isPlay": false,
    });
  },

  // 播放录音函数
  playRecord: function () {

    console.log('playRecord');
    var that = this;
    that.setData({
      "isPlay": true,
    });

  },

  // 获取下一句
  getNext: function () {

    // 切换到下一句字幕
    var that = this;
    that.setData({
      "index": that.data.index + 1
    });

  },

  // 重录函数
  remake: function () {

    console.log('remake');
  },

  // 提交录音
  submit: function () {

    console.log('submit');
    // 弹出提示框提示正在上传
    wx.showLoading({
      title: '文件上传中...',
      mask: true,
    })

    setTimeout(function () {
      wx.hideLoading();

      // 上传成功
      wx.showToast({
        title: '上传成功',
        icon: 'success',
        mask: true,
        duration: 3000,
        success: function () {
          // 跳转到得分页面
          setTimeout(function () {
            wx.navigateTo({
              url: "/pages/score/score",
            })
          })
        }
      });

    }, 2000)

  },

  // 去排行榜页面
  gotoRanklist: function () {

    wx.navigateTo({
      url: "/pages/rankList/rankList",
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },



  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})