// pages/my/my.js

var util = require('../../utils/util.js')
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    // loginStatus: app.globalData.loginStatus,
    loginStatus: true,
    dateTime: "",
    todayDate: "",
    LearningDays:null,
    PracticeCounts:null,
    PointPraiseCounts:null,
    recordList: [{ RankingNumbers: 1, Score: 86.2, PlayTimes: 520, PointPraise: 233 }, { RankingNumbers: 6, Score: 76.2, PlayTimes: 1520, PointPraise: 433 }],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var self = this

    console.log(app.globalData.userInfo)

    self.setData({
      userInfo: app.globalData.userInfo,
    })

    var nowDate = new Date();
    var str = nowDate.getFullYear() + "-" + nowDate.getMonth() + "-" + nowDate.getDate();
    self.setData({
      dateTime: str,
      todayDate: str,
    })
  },

  onShow: function (options) {
    var self = this


    if (app.globalData.loginStatus) {

      //获取当前日期数据
      var frams = {
        data: {
          AppKey: "049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4",
          OpenId: app.globalData.openid,
          DateTime: self.data.dateTime,
        },
      };
      api.methodGetUserLearningRecords({
        frams,
        success: (res) => {
          console.log(res);
          if (res.data.code != 0) {
            wx.showModal({
              title: '温馨提示',
              showCancel: false,
              content: res.data.msg,
            })
            return;
          } else {
            //刷新当前界面数据
            self.setData({
              LearningDays: res.data.LearningDays,
              PracticeCounts: res.data.PracticeCounts,
              PointPraiseCounts: res.data.PointPraiseCounts,
              recordList: res.data.LearningRecordList,
            });
          }
        },
        fail: (res) => {
          console.log(res);
        },
        complete: (res) => {
          console.log(res);
        }
      });
    }
  },

  tapLogin: function (event) {
    wx.navigateTo({
      url: '../login/login',
    })
  },

  tapRegist: function (event) {
    wx.navigateTo({
      url: '../regist/regist',
    })
  },

  quitLogin: function (event) {

    var self = this;

    //清除登录用户信息 

    self.setData({
      loginStatus: false
    })
  },

  bindDateChange: function (event) {
    var self = this;

    self.setData({
      dateTime: event.detail.value,
    })

    //获取指定日期数据
    var frams = {
      data: {
        AppKey: "049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4",
        OpenId: app.globalData.openid,
        DateTime: self.data.dateTime,
      },
    };
    api.methodGetUserLearningRecords({
      frams,
      success: (res) => {
        console.log(res);
        if (res.data.code != 0) {
          wx.showModal({
            title: '温馨提示',
            showCancel: false,
            content: res.data.msg,
          })
          return;
        } else {
          //刷新当前界面数据
          self.setData({
            LearningDays: res.data.LearningDays,
            PracticeCounts: res.data.PracticeCounts,
            PointPraiseCounts: res.data.PointPraiseCounts,
            recordList: res.data.LearningRecordList,
          });
        }
      },
      fail: (res) => {
        console.log(res);
      },
      complete: (res) => {
        console.log(res);
      }
    });
  },
})