var urlKYXContent = 'ECI/kouyuxiu/IES_base.ashx';
var urlWJWContent = 'ECI/waijiao/User_Base.ashx';
// var urlContent = 'ECI/TOEFL_IEITS/TO_IE_Base.ashx?AppKey=049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4'

var wxRequest = (params, url) => {
  wx.request({
    url,
    method: params.method || 'POST',
    data: params.data || {},
    header: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    success(res) {
      if (params.success) {
        params.success(res);
      }
    },
    fail(res) {
      if (params.fail) {
        params.fail(res);
      }
    },
    complete(res) {
      if (params.complete) {
        params.complete(res);
      }
    },
  });
};

var methodSession = (params) => {
  wxRequest(params, `${getApp().globalData.apiUrl}/${urlWJWContent}`);
};

var methodWechatGetSMSCode = (params) => {
  wxRequest(params, `${getApp().globalData.apiUrl}/${urlWJWContent}`);
};

var methodWechatRegester = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlWJWContent}`);
};

var methodGetDailyUpdatesAndHotRecommendation = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlKYXContent}`);
};

var methodWechatStartLearning = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlKYXContent}`);
};

var methodWechatSumbitMyVoice = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlKYXContent}`);
};

var methodGetRankingList = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlKYXContent}`);
};

var methodWechatLogin = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlWJWContent}`);
};

var methodGetUserLearningRecords = (params) => {
  wxrequest(params, `${getApp().globalData.apiUrl}/${urlWJWContent}`);
};

module.exports = { 
  methodSession,
  methodWechatGetSMSCode,
  methodWechatRegester,
  methodGetDailyUpdatesAndHotRecommendation,
  methodWechatStartLearning,
  methodWechatSumbitMyVoice,
  methodGetRankingList,
  methodWechatLogin,
  methodGetUserLearningRecords,
};