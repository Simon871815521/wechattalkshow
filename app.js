//app.js

var api = require('utils/api.js');

App({
  onLaunch: function () {
    var self = this

    if (self.globalData.session_key){
      wx.checkSession({
        success: function (res) {
          //session 未过期，并且在本生命周期一直有效
        },
        fail: function (res) {
          console.log(res);
          session.clear();
          //重新登录
          self.getSession();
        },
        complete: function (res) {
          console.log(res);
          self.getSession();
        }
      });
    } else {

    }
  },

  //登录并获取新的session
  getSession: function () {
    var self = this
    //调用登录接口
    wx.login({
      success: function (data) {

        self.globalData.wxCode = data.code;

        var params = {
          data: {
            appid: self.globalData.appId,
            secret: self.globalData.appSecret,
            js_code: self.code,
          },
        };

        //通过传code给服务器，调用微信接口获取用户openid
        api.methodSession({
          params,
          success: (res) => {
            console.log(res);
            self.globalData.openid = res.data.openid;
            self.globalData.session_key = res.data.session_key;
          },
          fail: (res) => {
            console.log('拉取用户openid失败，将无法正常使用开放接口等服务', res)
          },
          complete: (res) => {
            console.log(res);
          }
        });

        //获取用户信息
        wx.getUserInfo({
          success: function (res) {
            self.globalData.userInfo = res.userInfo;
            typeof cb == "function" && cb(self.globalData.userInfo)
          },
          fail: function (res) {
            console.log('wx.getUserInfo 接口调用失败，将无法获取用户信息', res)
          }
        })
      },
      fail: function (err) {
        console.log('wx.login 接口调用失败，将无法正常使用开放接口等服务', err)
      }
    });
  },

  getUserInfo: function (cb) {
    var self = this;
    if (self.globalData.userInfo) {
      typeof cb == "function" && cb(self.globalData.userInfo)
    } else {
      //调用登录接口
      wx.login({
        success: function (data) {
          //获取用户信息
          wx.getUserInfo({
            success: function (res) {
              self.globalData.userInfo = res.userInfo;
              typeof cb == "function" && cb(self.globalData.userInfo)
            },
            fail: function (res) {
              console.log('wx.getUserInfo 接口调用失败，将无法获取用户信息', res)
            }
          })
        },
        fail: function (err) {
          console.log('wx.login 接口调用失败，将无法正常使用开放接口等服务', err)
        }
      });
    }
  },

  globalData: {
    userInfo: null,
    certificationOk: 0,//是否认证
    appId: 'wx820493357a58f254',//微信小程序的appId
    appSecret: '5a6ec00b69f935b4453934a73e5de1c2',//微信小程序的密钥
    appkey: '049BD15C6FC04BD80808A601DC46E50515CBEEA33FB29AB4',
    wxCode: null,
    session_key: "1",
    openId: null,
    loginStatus: false,
    // apiUrl: "https://www.acadsoc.com",
    apiUrl: "http://192.168.74.98",
    timer: 30000,//定时器设置时间
    access_token: null,
  },

})



